# Ultrasonic Android CI

This project contains Android images for use in the Ultrasonic continuous
integration system.

It contains the Android SDK without any propiertary Google Play components and most common packages necessary for building Android apps in a CI tool like GitLab CI. 
It might be useful for other Open Source projects as a base for their CI.

### Usage
Make sure your CI environment's caching is working!
Normally Gradle will store part of its cache in ~/.gradle/ but only files from the working directory are cacheable in Gitlab CI, so it's necessary to set $GRADLE_USER_HOME

A `.gitlab-ci.yml` with caching of your project's dependencies would look like this:

```
image: registry.gitlab.com/ultrasonic/ci-android:latest

stages:
- build

before_script:
- export GRADLE_USER_HOME=$(pwd)/.gradle
- chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
  - .gradle/

build:
  stage: build
  script:
  - ./gradlew assembleDebug
  artifacts:
    paths:
    - app/build/outputs/apk/app-debug.apk
```
